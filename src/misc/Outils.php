<?php

namespace mywishlist\misc;

use mywishlist\models\User;
use mywishlist\models\Message;

class Outils
{
    public static function headerHTML($titre)
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $css = $rootUri . '/web/css/site.css';
        $js = $rootUri . '/web/js/site.js';
        $registerURL = $rootUri . '/inscription';
        $userListsURL = $rootUri . '/vos-listes';
        $newListURL = $rootUri . '/nouvelle-liste';
        $loginURL = $rootUri . '/login';
        $logoutURL = $rootUri . '/logout';
        $stringRes = <<< EOF
<!DOCTYPE html>
<html lang="fr">
        
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="screen" href="$css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script
		src="https://code.jquery.com/jquery-3.4.1.js"
		integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
    </script>
    <script src="$js"></script>
    <title>$titre</title>
</head>
        
<body>
        
    <div id="wrapper">

        <div id="header">
            <a href="$rootUri">mywishlist</a>
        </div>
        <nav id="navigation">
            <a id="home" href="$rootUri">Accueil</a>
            <a id="newListNav" href="$newListURL">Créer une liste</a>

EOF;

        if (isset($_SESSION['profile'])) {
            $uid = $_SESSION['profile']['user_id'];
            $username = $_SESSION['profile']['username'];
            $avatar = $rootUri . '/web/images/avatars/';
            $avatar .= User::where('user_id', '=', $uid)->first()->avatar;
            $profilURL = $app->urlFor('profil', ['id' => $uid]);

            $stringRes .= <<< EOF
            <a href="$userListsURL">Vos listes</a>
            <a id="user" href="$profilURL">
                <img id="avatar" src="$avatar" alt="">
                <span>$username</span>
            </a>
            <a id ="logout" href="$logoutURL">
                <span>Déconnexion</span>
                <i class="fas fa-sign-out-alt"></i>
            </a>
EOF;
        } else {
            $stringRes .= <<< EOF
            <div id="loginDropdown">
                <button id="loginButton">Login</button>
                <div id="loginDropdownContent">
                    <form method="Post" action="$loginURL">
                        <h1>Login</h1>
                        <input name="username" type="text" placeholder="Nom d'utilisateur" required />
                        <input name="password" type="password" placeholder="Mot de passe" required />
                        <input type="submit" value="Login" />
                    </form>
                </div>  
            </div>
            <a id="registerButton" href="$registerURL">Inscription</a>
       
EOF;
        }

        $stringRes .= "\n\t\t</nav>\n\n";

        print $stringRes;
    }

    public static function loginFormHTML()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $registerURL = $rootUri . '/inscription';
        $stringRes = "";
        if (isset($_SESSION['slim.flash']['username']))
            $username = $_SESSION['slim.flash']['username'];
        else
            $username = "";

        if (isset($_SESSION['slim.flash']['messageBox'])) {
            $message = $_SESSION['slim.flash']['messageBox'];
            $stringRes .= <<< EOF
                <span id="messageBox" class="loginMessage">$message</span>
    
EOF;
        }

        $stringRes .= <<< EOF
        <form class="form" method ="Post">
            <h1>Login</h1>
            <span id="signIn">Pas de compte ? <a href="$registerURL">Inscrivez-vous</a></span>

EOF;
        if (isset($_SESSION['slim.flash']['idError'])) {
            $idError = $_SESSION['slim.flash']['idError'];
            $stringRes .= <<< EOF
                <div class="idError">
                    <i class="fas fa-times-circle"></i>
                    <span>$idError</span>
                </div>

EOF;
        }
        $stringRes .= <<< EOF
            <div class="fieldName">
                <span>Nom d'utilisateur</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="username" type="text" value="$username" required/>
            <div class="fieldName">
                <span>Mot de passe</span>
                <span class="required">REQUIS</span>
            </div>
            <input name ="password" type="password" required/>
            <input type="submit" value="Login" />
        </form>

EOF;

        print $stringRes;
    }

    public static function registerFormHTML()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $loginURL = $rootUri . '/login';

        if (isset($_SESSION['slim.flash']['username']))
            $username = $_SESSION['slim.flash']['username'];
        else
            $username = "";
        $stringRes = <<< EOF
        <form class="form" method ="Post">
            <h1>Inscription</h1>
            <span id="signIn">Déjà inscrit ? <a href=$loginURL>Connectez-vous</a></span>
            <div class="fieldName">
                <span>Nom d'utilisateur</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="username" type="text" value="$username" required/>

EOF;
        if (isset($_SESSION['slim.flash']['usernameError'])) {
            $unameError = $_SESSION['slim.flash']['usernameError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$unameError</span>
            </div>

EOF;
        }

        $stringRes .= <<< EOF
            <div class="fieldName">
                <span>Mot de passe</span>
                <span class="required">REQUIS</span>
            </div>
            <input name ="password" type="password" required/>

EOF;
        if (isset($_SESSION['slim.flash']['passwordError'])) {
            $pwordError = $_SESSION['slim.flash']['passwordError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$pwordError</span>
            </div>

EOF;
        }

        $stringRes .= <<< EOF
            <div class="fieldName">
                <span>Confirmez mot de passe</span>
                <span class="required">REQUIS</span>
            </div>
            <input name ="rePassword" type="password" required/>

EOF;

        if (isset($_SESSION['slim.flash']['rePasswordError'])) {
            $rePwordError = $_SESSION['slim.flash']['rePasswordError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$rePwordError</span>
            </div>

EOF;
        }

        $stringRes .= "\t\t\t<input type=\"submit\" value=\"Créer mon compte\"/>\n\t\t</form>";

        print $stringRes;
    }

    public static function newListForm()
    {
        $date = date("Y-m-d");
        $stringRes = <<< EOF
        <form class="form" method ="Post">
            <h1 id="newList">Nouvelle liste</h1>
            <div class="fieldName">
                <span>Titre de la liste</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="listTitle" type="text" required/>
            <div class="fieldName">
                <span>Description</span>
            </div>
            <textarea name="listDesc" placeholder="Décrivez ici le but de votre wishlist !"></textarea>
            <div class="fieldName">
                <span>Date d'expiration</span>
                <span class="required">REQUIS</span>
            </div>
            <input type="date" name="expDate" min="$date" required>
            <input type="submit" value="Créer ma wishlist"/>
        </form>

EOF;

        print $stringRes;
    }

    public static function editListForm($list)
    {
        $list = $list->toArray();
        $date = date("Y-m-d");
        $listTitle = $list['titre'];
        $listDesc = $list['description'];
        $listExp = $list['expiration'];
        $stringRes = <<< EOF
        <form class="form" method ="Post">
            <h1 id="newList">Editer liste</h1>
            <div class="fieldName">
                <span>Titre de la liste</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="listTitle" type="text" value="$listTitle" required/>
            <div class="fieldName">
                <span>Description</span>
            </div>
            <textarea name="listDesc" placeholder="Décrivez ici le but de votre wishlist !">$listDesc</textarea>
            <div class="fieldName">
                <span>Date d'expiration</span>
                <span class="required">REQUIS</span>
            </div>
            <input type="date" name="expDate" value="$listExp" min="$date" required>
            <input type="submit" value="OK"/>
        </form>

EOF;

        print $stringRes;
    }

    public static function listModMenuHTML($list)
    {
        $app = \Slim\Slim::getInstance();
        $rootURI = $app->request()->getRootUri();
        $resourceURI = $app->request()->getResourceUri();
        $newItemURL = $rootURI . $resourceURI . '/ajouter-item';
        $list = $list->toArray();
        $backURL = $app->urlFor('liste', ['token' => $list['token_participation']]);
        $shareURL = $app->request()->getHost() . $backURL;
        $token = $list['token_modification'];
        $deleteURL = $app->urlFor('suppression-liste', ['token' => $token]);
        $listEditURL = $app->urlFor('editer-liste', ['token' => $token]);
        $togglePublicURL = $app->urlFor('toggle-public', ['token' => $token]);
        $listNum = $list['no'];
        $stringRes = <<< EOF
        <div id="listModMenu">
            <p id="listNum">Liste n° <strong>$listNum</strong></p>
            <a id="listBack" href="$backURL">
                <i class="fas fa-arrow-circle-left"></i>
                <p>Retour à la liste</p>
            </a>
            <a id="listEdit" href="$listEditURL">
                <i class="fas fa-cogs"></i>
                <p>Editer la liste</p>
            </a>
            <a id="addItem" href="$newItemURL">
                <i class="fas fa-gift"></i>
                <p>Ajouter un item</p>
            </a>
            <form method="Post" action="$deleteURL" id="listDelete" onsubmit="return confirm('Voulez-vous vraiment supprimer la liste ?');">
                <button class="listButton" type="submit">
                    <i class="fas fa-trash-alt"></i>
                    <p>Supprimer la liste</p>
                </button>
            </form>
            <button id="modalButton" class="listButton" type="submit">
                <i class="fas fa-share-square"></i>
                <p>Partager la liste</p>
            </button>
EOF;

        $public = $list['public'];
        if ($public) {
            $stringRes .= <<< EOF
            <a id="listPublic" href="$togglePublicURL">
                <i class="fas fa-lock"></i>
                <p>Rendre la liste privée</p>
            </a>
            <p id="publicInfo">Cette liste est <strong>publique</strong>.<br>Elle est visible par tout utilisateur du site et tout le monde peut y participer.</p>

EOF;
        } else {
            $stringRes .= <<< EOF
            <a id="listPublic" href="$togglePublicURL">
                <i class="fas fa-lock-open"></i>
                <p>Rendre la liste publique</p>
            </a>
            <p id="publicInfo">Cette liste est <strong>privée</strong>.<br>Seules les personnes à qui vous transmettez le lien de participation peuvent y accéder.</p>

EOF;
        }

        $stringRes .= <<< EOF
        </div>

        <div id="URLModal">
            <div id="modalContent">
                <div id ="modalUp">
                    <span>Copiez cet URL et partagez le pour inviter vos amis à consulter votre liste !</span>
                    <button id="closeModal">X</button>
                </div>
                <div id="urlCopy">
                    <input id="copyText" type="text" value ="$shareURL" readonly>
                    <button id="copyButton"><i class="fas fa-copy"></i></button>
                </div>
            </div>
        </div>

EOF;

        print $stringRes;
    }

    public static function newItemHTML()
    {
        if (isset($_SESSION['slim.flash']['itemName']))
            $itemName = $_SESSION['slim.flash']['itemName'];
        else
            $itemName = "";

        if (isset($_SESSION['slim.flash']['itemDesc']))
            $itemDesc = $_SESSION['slim.flash']['itemDesc'];
        else
            $itemDesc = "";

        if (isset($_SESSION['slim.flash']['itemCost']))
            $itemCost = $_SESSION['slim.flash']['itemCost'];
        else
            $itemCost = "";

        if (isset($_SESSION['slim.flash']['imgURL']))
            $imgURL = $_SESSION['slim.flash']['imgURL'];
        else
            $imgURL = "";

        if (isset($_SESSION['slim.flash']['itemURL']))
            $itemURL = $_SESSION['slim.flash']['itemURL'];
        else
            $itemURL = "";

        $stringRes = <<< EOF
        <form class="form" method ="Post">
            <h1 id="newList">Nouvel item</h1>
            <div class="fieldName">
                <span>Nom</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="itemName" value="$itemName" type="text" required/>
            <div class="fieldName">
                <span>Description</span>
            </div>
            <textarea name="itemDesc" placeholder="Description plus complète de l'item !">$itemDesc</textarea>
            <div class="fieldName">
                <span>Tarif (en €)</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="itemCost" value="$itemCost" type="text" required/>
EOF;
        if (isset($_SESSION['slim.flash']['costError'])) {
            $costError = $_SESSION['slim.flash']['costError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$costError</span>
            </div>

EOF;
        }
        $stringRes .= <<< EOF
            <div class="fieldName">
                <span>Image (sous forme d'URL)</span>
            </div>
            <input type="text" value="$imgURL" name="imgURL" list="images"/>
            
            <div class="fieldName">
                <span>Page web du produit</span>
            </div>
            <input name="itemURL" value="$itemURL" type="url"/>
            <input type="submit" value="Ajouter cet item"/>
        </form>

EOF;

        // <datalist id="images">
        //     <option>Volvo</option>
        //     <option>Saab</option>
        //     <option>Mercedes</option>
        //     <option>Audi</option>
        // </datalist>
        print $stringRes;
    }

    public static function editItemForm($item, $token)
    {
        $item = $item->toArray();
        $nom = $item['nom'];
        $desc = $item['descr'];
        $tarif = $item['tarif'];
        $imgURL = $item['img'];
        $url = $item['url'];

        if (isset($_SESSION['slim.flash']['itemName']))
            $itemName = $_SESSION['slim.flash']['itemName'];
        else
            $itemName = $nom;

        if (isset($_SESSION['slim.flash']['itemDesc']))
            $itemDesc = $_SESSION['slim.flash']['itemDesc'];
        else
            $itemDesc = $desc;

        if (isset($_SESSION['slim.flash']['itemCost']))
            $itemCost = $_SESSION['slim.flash']['itemCost'];
        else
            $itemCost = $tarif;

        if (isset($_SESSION['slim.flash']['imgURL']))
            $imgURL = $_SESSION['slim.flash']['imgURL'];
        else
            $imgURL = $imgURL;

        if (isset($_SESSION['slim.flash']['itemURL']))
            $itemURL = $_SESSION['slim.flash']['itemURL'];
        else
            $itemURL = $url;

        $stringRes = <<< EOF
        <form class="form" method ="Post">
            <h1 id="newList">Editer item</h1>
            <div class="fieldName">
                <span>Nom</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="itemName" value="$itemName" type="text" required/>
            <div class="fieldName">
                <span>Description</span>
            </div>
            <textarea name="itemDesc" placeholder="Description plus complète de l'item !">$itemDesc</textarea>
            <div class="fieldName">
                <span>Tarif (en €)</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="itemCost" value="$itemCost" type="text" required/>
EOF;
        if (isset($_SESSION['slim.flash']['costError'])) {
            $costError = $_SESSION['slim.flash']['costError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$costError</span>
            </div>

EOF;
        }
        $stringRes .= <<< EOF
            <div class="fieldName">
                <span>Image (sous forme d'URL)</span>
            </div>
            <input type="text" value="$imgURL" name="imgURL" list="images"/>
            
            <div class="fieldName">
                <span>Page web du produit</span>
            </div>
            <input name="itemURL" value="$itemURL" type="url"/>
            <input type="submit" value="OK"/>
        </form>

EOF;

        print $stringRes;
    }

    public static function reservationForm($item, $token)
    {

        if (isset($_SESSION['profile']))
            $participant = $_SESSION['profile']['username'];
        else {
            if (isset($_SESSION['participant']))
                $participant = $_SESSION['participant'];
            else
                $participant = "";
        }

        $item = $item->toArray();
        $itemNom = $item['nom'];
        $itemCost = $item['tarif'];

        $app = \Slim\Slim::getInstance();
        $backURL = $app->urlFor('liste', ['token' => $token]);

        $stringRes = <<< EOF
        <a class="backButton" href=$backURL>Retour à la liste</a>
        <div id="reservationInfo">
            <span>Vous allez réserver <strong>$itemNom</strong> à </span><span class ="tarif">$itemCost €</span><span>.</span>
        </div>
        <form class="form" method ="Post">
            <h1 id="newList">Reserver un item</h1>
            <div class="fieldName">
                <span>Votre nom</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="nomParticipant" type="text" value="$participant" required/>
            <div class="fieldName">
                <span>Message</span>
            </div>
            <textarea name="message" placeholder="Ecrivez un message pour accompagner votre cadeau !"></textarea>
            <input type="submit" value="Reserver le cadeau" />
        </form>

EOF;

        print $stringRes;
    }


    public static function listMessages($list)
    {

        $list = $list->toArray();

        if (isset($_SESSION['profile']))
            $participant = $_SESSION['profile']['username'];
        else {
            if (isset($_SESSION['participant']))
                $participant = $_SESSION['participant'];
            else
                $participant = "";
        }

        $stringRes = <<< EOF
        <div id ="listMessageForm">
            <form class="form" id="messageForm" method ="Post">
                <h1 id="newList">Laisser un message</h1>
                <div class="fieldName">
                    <span>Nom</span>
                    <span class="required">REQUIS</span>
                </div>
                <input name="messageName" type="text" value="$participant" required/>
                <div class="fieldName">
                    <span>Message</span>
                </div>
                <textarea name="messageTxt" placeholder="Ecrivez votre message ici"></textarea>
                <input type="submit" value="Publier le message" />
            </form>
        </div>
    </div>

EOF;

        print $stringRes;
    }

    public static function poolFormHTML($item, $token)
    {

        if (isset($_SESSION['profile']))
            $participant = $_SESSION['profile']['username'];
        else {
            if (isset($_SESSION['participant']))
                $participant = $_SESSION['participant'];
            else
                $participant = "";
        }

        $item = $item->toArray();
        $itemNom = $item['nom'];
        $itemCost = $item['tarif'];
        $restant = $itemCost - $item['cagnotte_montant'];

        $app = \Slim\Slim::getInstance();
        $backURL = $app->urlFor('liste', ['token' => $token]);

        $stringRes = <<< EOF
        <a class="backButton" href=$backURL>Retour à la liste</a>
        <div id="reservationInfo">
            <span>Vous allez participer à l'achat de <strong>$itemNom</strong> à </span><span class ="tarif">$itemCost €</span><span>.</span>
        </div>
        <form class="form" method ="Post">
            <h1 id="newList">Contribuer à un item</h1>
            <div class="fieldName">
                <span>Montant de la participation (max $restant €)</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="itemParticipation" value="$restant" type="text" required/>

EOF;

        if (isset($_SESSION['slim.flash']['costError'])) {
            $costError = $_SESSION['slim.flash']['costError'];
            $stringRes .= <<< EOF
    <div class="error">
        <i class="fas fa-times-circle"></i>
        <span>$costError</span>
    </div>

EOF;
        }

        $stringRes .= <<< EOF
            <input type="submit" value="Participer au cadeau" />
        </form>

EOF;

        print $stringRes;
    }


    public static function userPageHTML($id)
    {
        $app = \Slim\Slim::getInstance();
        $passwordURL = $app->urlFor('changer-mdp', ['id' => $id]);
        $user = User::find($id);
        $username = $user->username;

        $stringRes = <<< EOF
        <div id="userPage">
            <p id="profileHeader">Profil de <strong>$username</strong></p>
            <a href="$passwordURL" id="passwordButton">Changer mot de passe</a>
        </div>

EOF;

        print $stringRes;
    }

    public static function changePasswordForm()
    {
        $stringRes = <<< EOF
        <form class="form" method ="Post">
            <h1 id="newList">Changement de mot de passe</h1>
            <div class="fieldName">
                <span>Ancien mot de passe</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="oldPassword" type="password" required/>

EOF;

        if (isset($_SESSION['slim.flash']['oldPasswordError'])) {
            $oldPasswordError = $_SESSION['slim.flash']['oldPasswordError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$oldPasswordError</span>
            </div>

EOF;
        }
        $stringRes .= <<< EOF
            <div class="fieldName">
                <span>Nouveau mot de passe</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="newPassword" type="password" required/>

EOF;

        if (isset($_SESSION['slim.flash']['newPasswordError'])) {
            $newPasswordError = $_SESSION['slim.flash']['newPasswordError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$newPasswordError</span>
            </div>

EOF;
        }

        $stringRes .= <<< EOF
            <div class="fieldName">
                <span>Confirmez mot de passe</span>
                <span class="required">REQUIS</span>
            </div>
            <input name="confirmPassword" type="password" required/>

EOF;

        if (isset($_SESSION['slim.flash']['confirmPasswordError'])) {
            $confirmPasswordError = $_SESSION['slim.flash']['confirmPasswordError'];
            $stringRes .= <<< EOF
            <div class="error">
                <i class="fas fa-times-circle"></i>
                <span>$confirmPasswordError</span>
            </div>

EOF;
        }

        $stringRes .= <<< EOF
            <input type="submit" value="Changer mot de passe"/>

EOF;

        print $stringRes;
    }

    public static function footerHTML()
    {
        print <<< EOF

    </div>  
</body>
</html>
EOF;
    }
}
