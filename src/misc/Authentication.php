<?php

namespace mywishlist\misc;

use PasswordPolicy\PolicyBuilder;
use PasswordPolicy\Policy;
use PasswordPolicy\Validator;
use mywishlist\models\User;

define("REGULAR_USER", 1);

class Authentication
{
    public static function createUser($username, $password)
    {
        $unameError = $pwordError = "";
        $builder = new PolicyBuilder(new Policy);
        $builder->minLength(6)->upperCase(1)->digits(1);
        $validator = new Validator($builder->getPolicy());

        if (!ctype_alnum($username))
            $unameError = "Le nom d'utilisateur ne doit contenir que des caractères alphanumériques.";
        elseif (User::where('username', '=', $username)->exists())
            $unameError = "Ce nom d'utilisateur est déjà pris.";

        if (!($validator->attempt($password)))
            $pwordError = "Le mot de passe doit faire 6 caractères ou plus et contenir au moins une majuscule et un chiffre.";

        if ($unameError == "" && $pwordError == "") {
            $newUser = new User();
            $newUser->username = $username;
            $newUser->password = password_hash($password, PASSWORD_DEFAULT);
            $newUser->role_id = REGULAR_USER;
            $newUser->avatar = 'default.png';
            $newUser->save();
            Authentication::authenticate($username, $password);
            return 1;
        } else {
            return array('usernameError' => $unameError, 'passwordError' => $pwordError);
        }
    }

    public static function authenticate($username, $password)
    {
        if (User::where('username', '=', $username)->exists()) {
            $user = User::where('username', '=', $username)->first();
            if (password_verify($password, $user->password)) {
                Authentication::loadProfile($user->user_id);
                return true;
            }
        }
        return false;
    }

    private static function loadProfile($uid)
    {
        $user = User::where('user_id', '=', $uid)->first();
        $profile = array(
            'username' => $user->username,
            'user_id' => $uid,
            'role_id' => $user->role_id,
            'role_label' => $user->role()->first()->role_label,
            'auth_level' => $user->role()->first()->auth_level
        );
        $_SESSION['profile'] = $profile;
    }

    public static function changePassword($uid, $newPassword)
    {
        $user = User::where('user_id', '=', $uid)->first();

        $builder = new PolicyBuilder(new Policy);
        $builder->minLength(6)->upperCase(1)->digits(1);
        $validator = new Validator($builder->getPolicy());

        if (!($validator->attempt($newPassword)))
            return false;
        else {
            $user->password = password_hash($newPassword, PASSWORD_DEFAULT);
            $user->save();
            return true;
        }
    }
}
