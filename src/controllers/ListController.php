<?php

namespace mywishlist\controllers;

use mywishlist\misc\Outils;
use mywishlist\views\UserView;
use mywishlist\models\Liste;
use mywishlist\views\MessageView;
use mywishlist\models\Message;
use Illuminate\Support\Facades\DB;

class ListController
{
    public function newListForm()
    {
        Outils::headerHTML('Création liste');
        Outils::newListForm();
        Outils::footerHTML();
    }

    public function createList()
    {
        $app = \Slim\Slim::getInstance();
        $listTitle = filter_var($app->request()->post('listTitle'), FILTER_SANITIZE_STRING);
        $listDesc = filter_var($app->request()->post('listDesc'), FILTER_SANITIZE_STRING);
        $expDate = filter_var($app->request()->post('expDate'), FILTER_SANITIZE_STRING);

        $modifToken = bin2hex(random_bytes(10));
        $partToken = bin2hex(random_bytes(10));

        $newList = new Liste();
        $newList->titre = $listTitle;
        $newList->description = $listDesc;
        $newList->expiration = $expDate;
        $newList->token_modification = $modifToken;
        $newList->token_participation = $partToken;

        if (isset($_SESSION['profile'])) {
            $uid = $_SESSION['profile']['user_id'];
            $newList->user_id = $uid;
        }

        $newList->save();

        $listURL = $app->urlFor('liste-modif', ['token' => $modifToken]);

        if (isset($_COOKIE['listCreator'])) {
            $cookieData = json_decode($_COOKIE['listCreator']);
            array_push($cookieData, $newList->no);
            setcookie('listCreator', json_encode($cookieData), 2147483647);
        } else {
            setcookie('listCreator', json_encode(array($newList->no)), 2147483647);
        }

        $app->response()->redirect($listURL);
    }

    public function editList($token)
    {
        $app = \Slim\Slim::getInstance();
        $listTitle = filter_var($app->request()->post('listTitle'), FILTER_SANITIZE_STRING);
        $listDesc = filter_var($app->request()->post('listDesc'), FILTER_SANITIZE_STRING);
        $expDate = filter_var($app->request()->post('expDate'), FILTER_SANITIZE_STRING);

        $list = Liste::where('token_modification', '=', $token)->first();
        $list->titre = $listTitle;
        $list->description = $listDesc;
        $list->expiration = $expDate;
        $list->save();

        $listURL = $app->urlFor('liste-modif', ['token' => $token]);

        $app->flash('messageBox', "La liste a bien été éditée.");
        $app->response()->redirect($listURL);
    }

    public function loadListModPage($token)
    {
        $app = \Slim\Slim::getInstance();
        $list = Liste::where('token_modification', '=', $token)->first();

        if ($list->user_id != 0) {
            if (isset($_SESSION['profile'])) {
                if ($_SESSION['profile']['user_id'] == $list->user_id) {
                    $v = new UserView($list, LIST_MOD_VIEW);
                    Outils::headerHTML('Modifier liste');
                    Outils::listModMenuHTML($list);
                    $v->render();
                    Outils::footerHTML();
                } else {
                    $app->flash('messageBox', "Cette liste appartient à un autre utilisateur.");
                    $app->response()->redirect($app->urlFor('accueil'));
                }
            } else {
                $app->flash('messageBox', "Cette liste appartient à un autre utilisateur. Si vous êtes le créateur de cette liste, connectez-vous et réessayez d'y accéder.");
                $app->response()->redirect($app->urlFor('accueil'));
            }
        } else {
            $v = new UserView($list, LIST_MOD_VIEW);
            Outils::headerHTML('Modifier liste');
            Outils::listModMenuHTML($list);
            $v->render();
            Outils::footerHTML();
        }
    }

    public function loadListPage($token)
    {
        $list = Liste::where('token_participation', '=', $token)->first();
        $messages = $list->messages()->OrderBy('created_at', 'DESC')->get();
        $mv = new MessageView($messages);
        $v = new UserView($list, LIST_VIEW);
        Outils::headerHTML($list->titre);
        $v->render();
        Outils::listMessages($list);
        $mv->render();
        Outils::footerHTML();
    }

    public function addMessage($token)
    {
        $app = \Slim\Slim::getInstance();
        $messageName = filter_var($app->request()->post('messageName'), FILTER_SANITIZE_STRING);
        $messageTxt = filter_var($app->request()->post('messageTxt'), FILTER_SANITIZE_STRING);

        $_SESSION['participant'] = $messageName;

        $list = Liste::where('token_participation', '=', $token)->first();
        $listID = $list->no;

        $message = new Message();
        $message->liste_id = $listID;
        $message->nom = $messageName;
        $message->message = $messageTxt;
        $message->save();

        $app->flash('messageBox', "Votre message a bien été envoyé.");
        $app->response()->redirect($app->urlFor('liste', ['token' => $token]));
    }

    public function loadListEditForm($token)
    {
        $list = Liste::where('token_modification', '=', $token)->first();
        Outils::headerHTML('Editer liste');
        Outils::editListForm($list);
        Outils::footerHTML();
    }

    public function deleteList($token)
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $list = Liste::where('token_modification', '=', $token);
        $items = $list->first()->items()->get();
        foreach ($items as $item) {
            $item->reservation()->delete();
        }
        $list->first()->items()->delete();
        $list->first()->messages()->delete();
        $list->delete();

        $app->flash('messageBox', "La liste a bien été supprimée.");
        if (isset($_SESSION['profile']))
            $app->response()->redirect($rootUri . '/vos-listes');
        else
            $app->response()->redirect($rootUri . '/accueil');
    }

    public function togglePublic($token)
    {
        $app = \Slim\Slim::getInstance();
        $list = Liste::where('token_modification', '=', $token)->first();

        if ($list->public) {
            $list->public = 0;
            $app->flash('messageBox', "Votre liste est maintenant privée.");
        } else {
            $list->public = 1;
            $app->flash('messageBox', "Votre liste est maintenant publique.");
        }

        $list->save();

        $listURL = $app->urlFor('liste-modif', ['token' => $token]);
        $app->response()->redirect($listURL);
    }
}
