<?php

namespace mywishlist\controllers;

use mywishlist\misc\Outils;
use mywishlist\views\UserView;
use mywishlist\models\Liste;
use mywishlist\models\User;
use mywishlist\misc\Authentication;

class UserController
{

    public function loadHome()
    {
        $today = date("Y-m-d");
        $publiclists = Liste::where('public', '=', 1)
            ->where('expiration', '>=', $today)
            ->orderBy('expiration', 'ASC')->get();
        $v = new UserView($publiclists, PUBLIC_LISTS_VIEW);
        Outils::headerHTML('Accueil');
        $v->render();
        Outils::footerHTML();
    }

    public function loadUserLists()
    {
        $userLists = User::where('user_id', '=', $_SESSION['profile']['user_id'])->first()->lists()->OrderBy('expiration', 'ASC')->get();
        $v = new UserView($userLists, USER_LISTS_VIEW);
        Outils::headerHTML('Vos Listes');
        $v->render();
        Outils::footerHTML();
    }

    public function redirectHome()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $app->response()->redirect($rootUri . '/accueil');
    }

    public function linkList($token)
    {
        $app = \Slim\Slim::getInstance();
        if (isset($_SESSION['profile'])) {
            $list = Liste::where('token_modification', '=', $token)->first();
            $list->user_id = $_SESSION['profile']['user_id'];
            $list->save();

            $app->flash('messageBox', "La liste a bien été liée à votre compte.");
            $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
        }
    }

    public function loadProfile($id)
    {
        $app = \Slim\Slim::getInstance();

        if (isset($_SESSION['profile'])) {
            if ($_SESSION['profile']['user_id'] == $id) {
                Outils::headerHTML('Profil');
                Outils::userPageHTML($id);
                Outils::footerHTML();
            } else {
                $app->flash('messageBox', "Le profil auquel vous tentez d'accéder n'est pas le vôtre.");
                $app->response()->redirect($app->urlFor('accueil'));
            }
        } else {
            $app->flash('messageBox', "Il faut vous connecter pour accéder à votre profil.");
            $app->response()->redirect($app->urlFor('accueil'));
        }
    }

    public function changePasswordForm($id)
    {
        $app = \Slim\Slim::getInstance();

        if (isset($_SESSION['profile'])) {
            if ($_SESSION['profile']['user_id'] == $id) {
                Outils::headerHTML('Profil');
                Outils::changePasswordForm();
                Outils::footerHTML();
            } else {
                $app->flash('messageBox', "Le profil auquel vous tentez d'accéder n'est pas le vôtre.");
                $app->response()->redirect($app->urlFor('accueil'));
            }
        } else {
            $app->flash('messageBox', "Il faut vous connecter pour accéder à votre profil.");
            $app->response()->redirect($app->urlFor('accueil'));
        }
    }

    public function changePassword($id)
    {
        $app = \Slim\Slim::getInstance();

        $user = User::find($id);
        $username = $user->username;
        $oldPassword = filter_var($app->request()->post('oldPassword'), FILTER_SANITIZE_STRING);
        if (Authentication::authenticate($username, $oldPassword)) {
            $newPassword = filter_var($app->request()->post('newPassword'), FILTER_SANITIZE_STRING);
            $confirmPassowrd = filter_var($app->request()->post('confirmPassword'), FILTER_SANITIZE_STRING);

            if ($newPassword != $confirmPassowrd) {
                $app->flash('confirmPasswordError', "Les deux mot de passe doivent être identiques.");
                $app->response()->redirect($app->urlFor('changer-mdp', ['id' => $id]));
            } else {
                if (Authentication::changePassword($id, $newPassword)) {
                    $app->flash('messageBox', "Votre mot de passe a bien été changé. Veuillez vous reconnecter.");
                    unset($_SESSION['profile']);
                    $app->response()->redirect($app->urlFor('login'));
                } else {
                    $app->flash('newPasswordError', "Le mot de passe doit faire 6 caractères ou plus et contenir au moins une majuscule et un chiffre.");
                    $app->response()->redirect($app->urlFor('changer-mdp', ['id' => $id]));
                }
            }
        } else {
            $app->flash('oldPasswordError', "Ce mot de passe est incorrect.");
            $app->response()->redirect($app->urlFor('changer-mdp', ['id' => $id]));
        }
    }
}
