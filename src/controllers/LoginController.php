<?php

namespace mywishlist\controllers;

use mywishlist\misc\Outils;
use mywishlist\misc\Authentication;

class LoginController
{

    public function checkLogin()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $username = filter_var($app->request()->post('username'), FILTER_SANITIZE_STRING);
        $password = filter_var($app->request()->post('password'), FILTER_SANITIZE_STRING);
        $auth = Authentication::authenticate($username, $password);

        if ($auth == true) {
            $app->response()->redirect($rootUri . '/accueil');
        } else {
            $app->flash('username', $username);
            $app->flash('idError', 'Vos identifiants sont incorrects');
            $app->response()->redirect($rootUri . '/login');
        }
    }

    public function loadLogin()
    {
        Outils::headerHTML('Login');
        Outils::loginFormHTML();
        Outils::footerHTML();
    }

    public function logout()
    {
        unset($_SESSION['profile']);
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $app->response()->redirect($rootUri . '/accueil');
    }
}
