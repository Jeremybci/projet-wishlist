<?php
namespace mywishlist\controllers;

use mywishlist\misc\Outils;
use mywishlist\models\Liste;
use mywishlist\misc\Authentication;

class RegistrationController
{

    public function loadRegistration()
    {
        $app = \Slim\Slim::getInstance();

        if (isset($_SESSION['profile'])) {
            $app->flash('messageBox', 'Vous avez déjà un compte.');
            $app->response()->redirect($app->urlFor('accueil'));
        } else {
            Outils::headerHTML('Inscription');
            Outils::registerFormHTML();
            Outils::footerHTML();
        }
    }

    public function registrationLinkList($no)
    {
        $app = \Slim\Slim::getInstance();
        $_SESSION['creationFromMod'] = $no;

        $app->response()->redirect($app->urlFor('inscription'));
    }

    public function checkRegistration()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $username = filter_var($app->request()->post('username'), FILTER_SANITIZE_STRING);
        $password = filter_var($app->request()->post('password'), FILTER_SANITIZE_STRING);
        $rePassword = filter_var($app->request()->post('rePassword'), FILTER_SANITIZE_STRING);

        if ($password != $rePassword) {
            $app->flash('username', $username);
            $app->flash('rePasswordError', 'Les mots de passe sont doivent être identiques.');
            $app->response()->redirect($rootUri . '/inscription');
        } else {
            $registration = Authentication::createUser($username, $password);

            if ($registration == 1) {
                if (isset($_SESSION['creationFromMod'])) {
                    $list = Liste::find($_SESSION['creationFromMod']);
                    $list->user_id = $_SESSION['profile']['user_id'];
                    $list->save();
                    unset($_SESSION['creationFromMod']);
                }
                $app->flash('messageBox', "Votre compte a bien été créé.");
                $app->response()->redirect($rootUri . '/accueil');
            } else {
                $app->flash('username', $username);

                if (array_key_exists('usernameError', $registration)) {
                    $app->flash('usernameError', $registration['usernameError']);
                }

                if (array_key_exists('passwordError', $registration)) {
                    $app->flash('passwordError', $registration['passwordError']);
                }

                $app->response()->redirect($rootUri . '/inscription');
            }
        }
    }
}
