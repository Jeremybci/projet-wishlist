<?php

namespace mywishlist\controllers;

use mywishlist\misc\Outils;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Reservation;
use mywishlist\views\UserView;

class ItemController
{

    public function addItemForm()
    {
        Outils::headerHTML("Nouvel item");
        Outils::newItemHTML();
        Outils::footerHTML();
    }

    public function addItem($token)
    {
        $app = \Slim\Slim::getInstance();
        $itemNum = Liste::where('token_modification', '=', $token)->first()->no;
        $itemName = filter_var($app->request()->post('itemName'), FILTER_SANITIZE_STRING);
        $itemDesc = filter_var($app->request()->post('itemDesc'), FILTER_SANITIZE_STRING);
        $itemCost = filter_var($app->request()->post('itemCost'), FILTER_SANITIZE_STRING);
        $imgURL = filter_var($app->request()->post('imgURL'), FILTER_SANITIZE_URL);
        $itemURL = filter_var($app->request()->post('itemURL'), FILTER_SANITIZE_URL);

        if (!is_numeric($itemCost)) {
            $app->flash('costError', "Le tarif ne doit contenir que des chiffres.");
            $app->flash('itemName', $itemName);
            $app->flash('itemDesc', $itemDesc);
            $app->flash('itemCost', $itemCost);
            $app->flash('imgURL', $imgURL);
            $app->flash('itemURL', $itemURL);
            $app->response()->redirect($app->urlFor('ajout-item', ['token' => $token]));
        } else {

            if (empty($imgURL))
                $imgURL = 'default.png';

            $newItem = new Item();
            $newItem->nom = $itemName;
            $newItem->liste_id = $itemNum;
            $newItem->descr = $itemDesc;
            $newItem->tarif = $itemCost;
            $newItem->img = $imgURL;
            $newItem->url = $itemURL;

            $newItem->save();
            $app->flash('messageBox', "L'item a bien été ajouté.");
            $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
        }
    }

    public function editItem($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $itemName = filter_var($app->request()->post('itemName'), FILTER_SANITIZE_STRING);
        $itemDesc = filter_var($app->request()->post('itemDesc'), FILTER_SANITIZE_STRING);
        $itemCost = filter_var($app->request()->post('itemCost'), FILTER_SANITIZE_STRING);
        $imgURL = filter_var($app->request()->post('imgURL'), FILTER_SANITIZE_URL);
        $itemURL = filter_var($app->request()->post('itemURL'), FILTER_SANITIZE_URL);

        if (!is_numeric($itemCost)) {
            $app->flash('costError', "Le tarif ne doit contenir que des chiffres.");
            $app->flash('itemName', $itemName);
            $app->flash('itemDesc', $itemDesc);
            $app->flash('itemCost', $itemCost);
            $app->flash('imgURL', $imgURL);
            $app->flash('itemURL', $itemURL);
            $app->response()->redirect($app->urlFor('edition-item', ['token' => $token, 'id' => $id]));
        } else {

            if (empty($imgURL))
                $imgURL = 'default.png';

            $editedItem = Item::find($id);
            $editedItem->nom = $itemName;
            $editedItem->descr = $itemDesc;
            $editedItem->tarif = $itemCost;
            $editedItem->img = $imgURL;
            $editedItem->url = $itemURL;

            $editedItem->save();
            $app->flash('messageBox', "L'item a bien été édité.");
            $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token, 'id' => $id]));
        }
    }

    public function deleteItem($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $item = Item::find($id);

        if (is_null($item->reservation_id) && $item['cagnotte_montant'] == 0) {
            $item->delete();
            $app->flash('messageBox', "L'item a bien été supprimé.");
            $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
        } else {
            $app->flash('messageBox', "Il n'est plus possible de supprimer cet item.");
            $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
        }
    }

    public function loadEditItemForm($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $item = Item::find($id);

        if (is_null($item->reservation_id) && $item['cagnotte_montant'] == 0) {
            Outils::headerHTML('Editer item');
            Outils::editItemForm($item, $token);
            Outils::footerHTML();
        } else {
            $app->flash('messageBox', "Il n'est plus possible de modifier cet item.");
            $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
        }
    }

    public function loadReservationForm($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $item = Item::find($id);

        if (is_null($item->reservation_id) && !$item->cagnotte_active) {
            Outils::headerHTML("Réserver item");
            Outils::reservationForm($item, $token);
            Outils::footerHTML();
        } else {
            $app->flash('messageBox', "Il n'est plus possible de réserver cet item.");
            $app->response()->redirect($app->urlFor('liste', ['token' => $token]));
        }
    }

    public function reserveItem($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $nom = filter_var($app->request()->post('nomParticipant'), FILTER_SANITIZE_STRING);
        $message = filter_var($app->request()->post('message'), FILTER_SANITIZE_STRING);

        $_SESSION['participant'] = $nom;

        $reservation = new Reservation();
        $reservation->nom_participant = $nom;
        $reservation->message = $message;
        $reservation->save();

        $item = Item::find($id);
        $item->reservation_id = $reservation->id;
        $item->save();

        $app->flash('messageBox', "L'item a bien été réservé.");
        $app->response()->redirect($app->urlFor('liste', ['token' => $token]));
    }

    public function loadItemPage($id)
    {
        $item = Item::find($id);
        $v = new UserView($item, ITEM_VIEW);

        Outils::headerHTML($item->nom);
        $v->render();
        Outils::footerHTML();
    }

    public function togglePool($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $item = Item::find($id);

        if (is_null($item->reservation_id)) {
            if ($item->cagnotte_active) {
                if ($item->cagnotte_montant == 0) {
                    $item->cagnotte_active = 0;
                    $item->save();
                    $app->flash('messageBox', "La cagnotte a bien été désactivée.");
                    $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
                } else {
                    $app->flash('messageBox', "Une contribution a déjà été faite, la cagnotte ne peut plus être désactivée.");
                    $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
                }
            } else {
                $item->cagnotte_active = 1;
                $item->save();
                $app->flash('messageBox', "La cagnotte a bien été activée.");
                $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
            }
        } else {
            $app->flash('messageBox', "Cet item a déjà été réservé, la cagnotte ne peut pas être activée.");
            $app->response()->redirect($app->urlFor('liste-modif', ['token' => $token]));
        }
    }

    public function loadPoolForm($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $item = Item::find($id);

        if (is_null($item->reservation_id) && $item->cagnotte_active == 1 && $item->cagnotte_montant < $item->tarif) {
            Outils::headerHTML("Réserver item");
            Outils::poolFormHTML($item, $token);
            Outils::footerHTML();
        } else {
            $app->flash('messageBox', "Il n'est pas possible de participer à la cagnotte de cet item.");
            $app->response()->redirect($app->urlFor('liste', ['token' => $token]));
        }
    }

    public function poolContribution($id, $token)
    {
        $app = \Slim\Slim::getInstance();
        $itemParticipation = filter_var($app->request()->post('itemParticipation'), FILTER_SANITIZE_STRING);
        $item = Item::find($id);

        if (!is_numeric($itemParticipation)) {
            $app->flash('costError', "Le montant ne doit contenir que des chiffres.");
            $app->response()->redirect($app->urlFor('participer-item', ['token' => $token, 'id' => $id]));
        } else {
            $restant = $item->tarif - $item->cagnotte_montant;
            if ($itemParticipation > $restant) {
                $app->flash('costError', "Le montant ne doit pas dépasser $restant €.");
                $app->response()->redirect($app->urlFor('participer-item', ['token' => $token, 'id' => $id]));
            } else {
                $item->cagnotte_montant += $itemParticipation;
                $item->save();
                $app->flash('messageBox', "L'item a bien été édité.");
                $app->response()->redirect($app->urlFor('liste', ['token' => $token]));
            }
        }
    }
}
