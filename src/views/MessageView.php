<?php

namespace mywishlist\views;

class MessageView
{

    protected $item;

    public function __construct($item)
    {
        $this->item = $item;
    }

    private function listMessagesHTML($messages)
    {
        $stringRes = "";
        foreach ($messages as $message) {
            $messageName = $message->nom;
            $messageTxt = $message->message;
            $messageTxt = nl2br(e($messageTxt));
            $originalDate = $message['created_at'];
            $created = date("d/m/Y à H:i:s", strtotime($originalDate));
            $stringRes .= <<< EOF
            <div class="message">
                <span class="messageName"><strong>$messageName</strong></span>
                <span class="messageTxt">$messageTxt</span>
                <span class="messageDate"><i class="far fa-clock"></i> $created</span>
            </div>

EOF;
        }

        return $stringRes;
    }

    public function render()
    {

        $content = $this->listMessagesHTML($this->item);

        print <<< EOF

        <div id="listMessages">
            $content
        </div>

EOF;
    }
}
