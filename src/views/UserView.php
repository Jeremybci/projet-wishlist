<?php

namespace mywishlist\views;

define("USER_LISTS_VIEW", 1);
define("LIST_VIEW", 2);
define("ITEM_VIEW", 3);
define("PUBLIC_LISTS_VIEW", 4);
define("LIST_MOD_VIEW", 5);

class UserView
{
    protected $item, $selecteur;

    public function __construct($item, $selecteur)
    {
        $this->item = $item;
        $this->selecteur = $selecteur;
    }

    private function userListsHTML($lists)
    {
        $lists = $lists->toArray();
        $app = \Slim\Slim::getInstance();
        $stringRes = "";

        if (isset($_SESSION['slim.flash']['messageBox'])) {
            $message = $_SESSION['slim.flash']['messageBox'];
            $stringRes .= <<< EOF
            <span id="messageBox">$message</span>

EOF;
        }

        $stringRes .= <<< EOF
        <h1>Vos listes</h1>
        <div id="listListWrapper">

EOF;
        foreach ($lists as $key => $list) {
            $listURL = $app->urlFor('liste', ['token' => $list['token_participation']]);
            $titre = $list['titre'];
            $desc = $list['description'];
            $originalExp = $list['expiration'];
            $expiration = date("d/m/Y", strtotime($originalExp));
            $stringRes .= <<< EOF
            <a class="list" href="$listURL">
                <section class="listTitle">$titre</section>
                <section class="listDesc">$desc</section>
                <section class="listExp"><i class="far fa-calendar-alt"></i>$expiration</section>
            </a>

EOF;
        }
        $stringRes .= "\t\t</div>";
        return $stringRes;
    }

    private function publicListsHTML($lists)
    {
        $lists = $lists->toArray();
        $app = \Slim\Slim::getInstance();
        $stringRes = "";

        if (isset($_SESSION['slim.flash']['messageBox'])) {
            $message = $_SESSION['slim.flash']['messageBox'];
            $stringRes .= <<< EOF
            <span id="messageBox">$message</span>

EOF;
        }

        $stringRes .= <<< EOF
        <h1>Listes publiques</h1>
        <div id="listListWrapper">

EOF;
        foreach ($lists as $key => $list) {
            $listURL = $app->urlFor('liste', ['token' => $list['token_participation']]);
            $titre = $list['titre'];
            $originalExp = $list['expiration'];
            $expiration = date("d/m/Y", strtotime($originalExp));
            $stringRes .= <<< EOF
            <a class="list" href="$listURL">
                <section class="listTitle">$titre</section>
                <section class="listExp"><i class="far fa-calendar-alt"></i>$expiration</section>
            </a>

EOF;
        }
        $stringRes .= "\t\t</div>";
        return $stringRes;
    }

    private function listHTML($list)
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $imgUri = $rootUri . '/web/images/items';
        $items = $list->items()->get()->toArray();
        $list = $list->toArray();
        $stringRes = "<div id=\"listWrapper\">\n";
        $titre = $list['titre'];
        $listDesc = $list['description'];
        $originalExp = $list['expiration'];
        $expiration = date("d/m/Y", strtotime($originalExp));
        $tokenPart = $list['token_participation'];

        if (isset($_SESSION['slim.flash']['messageBox'])) {
            $message = $_SESSION['slim.flash']['messageBox'];
            $stringRes .= <<< EOF
            <span id="messageBox">$message</span>

EOF;
        }

        if (isset($_SESSION['profile'])) {
            if ($_SESSION['profile']['user_id'] == $list['user_id']) {
                $listModURL = $app->urlFor('liste-modif', ['token' => $list['token_modification']]);
                $stringRes .= <<< EOF
            <a id="modifList" href="$listModURL">Modifier la liste</a>

EOF;
            }
        }

        $stringRes .= <<< EOF
                <div id="listHeader">
                    <section class="listTitle">$titre</section>
                    <section class="listDesc">$listDesc</section>
                    <section class="listExp"><i class="far fa-calendar-alt"></i>$expiration</section>
                </div>
                <div class ="listContent">

EOF;
        foreach ($items as $item) {
            $img = $item['img'];
            $nom = $item['nom'];
            $tarif = $item['tarif'];
            $itemURL = $app->urlFor('item', ['token' => $tokenPart, 'id' => $item['id']]);

            $stringRes .= <<< EOF
            <div class="itemWrapper">
                <a class="itemLink" href="$itemURL">

EOF;

            if (is_null($item['reservation_id']) && $item['cagnotte_montant'] != $item['tarif']) {
                $stringRes .= <<< EOF
                    <div class="item">

EOF;
            } else {
                $stringRes .= <<< EOF
                    <div class="item itemReserve">

EOF;
            }
            if (filter_var($img, FILTER_VALIDATE_URL)) {
                $stringRes .= <<< EOF
                        <section class="itemImg"><img src="$img" alt="$nom"></section>

EOF;
            } else {
                $stringRes .= <<< EOF
                        <section class="itemImg"><img src="$imgUri/$img" alt="$nom"></section>

EOF;
            }
            $stringRes .= <<< EOF
                        <div class ="itemInfo">
                            <section class="nom">$nom</section>
                            <section class="tarif">$tarif €</section>

EOF;

            if ($item['cagnotte_active']) {
                $cagnotteLeft = $tarif - $item['cagnotte_montant'];
                $stringRes .= <<< EOF
                            <section class="poolLeft">($cagnotteLeft € restant)</section>

EOF;
            }

            $stringRes .= <<< EOF
                        </div>
                    </div>
                </a>

EOF;
            if (is_null($item['reservation_id'])) {
                $id = $item['id'];

                if ($item['cagnotte_active']) {
                    if ($item['cagnotte_montant'] < $item['tarif']) {
                        $poolURL = $app->urlFor('participer-item', ['token' => $tokenPart, 'id' => $id]);
                        $stringRes .= <<< EOF
                <a class="offer" href="$poolURL"><i class="fas fa-piggy-bank"></i> Participer</a>
            </div>
    
EOF;
                    } else {
                        $stringRes .= <<< EOF
                <div class="reserved">Item réservé</div>
            </div>

EOF;
                    }
                } else {
                    $reserveItem = $app->urlFor('reserver-item', ['token' => $tokenPart, 'id' => $id]);
                    $stringRes .= <<< EOF
                <a class="offer" href="$reserveItem">Réserver</a>
            </div>

EOF;
                }
            } else {
                $stringRes .= <<< EOF
                <div class="reserved">Item réservé</div>
            </div>

EOF;
            }
        }
        $stringRes .= "\t\t</div>\n";

        return $stringRes;
    }


    private function listModHTML($list)
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $imgUri = $rootUri . '/web/images/items';
        $items = $list->items()->get()->toArray();
        $list = $list->toArray();
        $stringRes = "<div id=\"listWrapper\">\n";
        $titre = $list['titre'];
        $listDesc = $list['description'];
        $originalExp = $list['expiration'];
        $expiration = date("d/m/Y", strtotime($originalExp));
        $token = $list['token_modification'];

        if (isset($_SESSION['slim.flash']['messageBox'])) {
            $message = $_SESSION['slim.flash']['messageBox'];
            $stringRes .= <<< EOF
            <span id="messageBox">$message</span>

EOF;
        }

        if ($list['user_id'] == 0) {
            if (isset($_SESSION['profile'])) {
                $linkList = $app->urlFor('lier-liste', ['token' => $token]);
                $stringRes .= <<< EOF
                <div id="linkList">
                    <span><i class="fas fa-exclamation-circle"></i> Cette liste n'est pas liée à votre compte. Clickez <a href="$linkList">ici</a> pour l'ajouter à vos listes.</span>
                </div>

EOF;
            } else {
                $inscriptionLink = $app->urlFor('inscription-lien', ['no' => $list['no']]);
                $stringRes .= <<< EOF
                <div id="linkList">
                    <span><i class="fas fa-exclamation-circle"></i> Vous n'avez pas de compte utilisateur. Notez bien l'URL de cette page ou vous ne pourrez plus y accéder !</span>
                    <span>En <a href="$inscriptionLink">créant un compte</a>, vous pourrez sauvegarder vos listes et n'aurez plus à conserver cet URL.</span>
                </div>

EOF;
            }
        }


        $stringRes .= <<< EOF
                <div id="listHeader">
                    <section class="listTitle">$titre</section>
                    <section class="listDesc">$listDesc</section>
                    <section class="listExp"><i class="far fa-calendar-alt"></i>$expiration</section>
                </div>
                <div class ="listContent">

EOF;
        foreach ($items as $item) {
            $img = $item['img'];
            $nom = $item['nom'];
            $tarif = $item['tarif'];
            $id = $item['id'];

            if (is_null($item['reservation_id']) && $item['cagnotte_montant'] == 0) {
                $stringRes .= <<< EOF
                    <div class="itemWrapper">
                        <div class="item">

EOF;
            } else {
                $stringRes .= <<< EOF
                    <div class="itemWrapper" title="Cet item a été reservé, il ne peut plus être modifié.">
                        <div class="item itemReserve">

EOF;
            }

            if (filter_var($img, FILTER_VALIDATE_URL)) {
                $stringRes .= <<< EOF
                            <section class="itemImg"><img src="$img" alt="$nom"></section>

EOF;
            } else {
                $stringRes .= <<< EOF
                            <section class="itemImg"><img src="$imgUri/$img" alt="$nom"></section>

EOF;
            }
            $deleteURL = $app->urlFor('suppression-item', ['id' => $id, 'token' => $token]);
            $editItemURL = $app->urlFor('edition-item', ['id' => $id, 'token' => $token]);
            $stringRes .= <<< EOF
                            <div class ="itemInfo">
                                <section class="nom">$nom</section>
                                <section class="tarif">$tarif €</section>

EOF;

            if ($item['cagnotte_active']) {
                $cagnotteLeft = $tarif - $item['cagnotte_montant'];
                $stringRes .= <<< EOF
                    <section class="poolLeft">($cagnotteLeft € restant)</section>

EOF;
            }

            $stringRes .= <<< EOF
                            </div>
                        </div>

EOF;

            if (is_null($item['reservation_id']) && $item['cagnotte_montant'] == 0) {
                $cagnotteURL = $app->urlFor('toggle-cagnotte', ['token' => $token, 'id' => $id]);
                $stringRes .= <<< EOF
                        <div class="itemActions">
                            <a class="editItem" title="Editer item" href="$editItemURL"><i class="fas fa-pencil-alt"></i></a>

EOF;

                if ($item['cagnotte_active']) {
                    $stringRes .= <<< EOF
                            <a class="cagnotteItem cagnotteGrise" title="Désactiver la cagnotte sur cet item" href="$cagnotteURL"><i class="fas fa-piggy-bank"></i></a>

EOF;
                } else {
                    $stringRes .= <<< EOF
                            <a class="cagnotteItem" title="Activer une cagnotte sur cet item" href="$cagnotteURL"><i class="fas fa-piggy-bank"></i></a>

EOF;
                }

                $stringRes .= <<< EOF
                            <form class="deleteItem" action="$deleteURL" title="Supprimer item" method="Post" onsubmit="return confirm('Voulez-vous vraiment supprimer cet item ?');">
                                <button type="submit"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </div>
                    </div>

EOF;
            } else {
                $stringRes .= <<< EOF
                        <div class="itemActions" title="Cet item a été reservé, il ne peut plus être modifié">
                            <i class="fas fa-lock"></i>
                        </div>
                    </div>

EOF;
            }
        }
        $stringRes .= <<< EOF
                </div>
            </div>

EOF;
        return $stringRes;
    }

    private function itemHTML($item)
    {
        $today = date('Y-m-d');
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $imgUri = $rootUri . '/web/images/items';
        $list = $item->liste()->first()->toArray();
        $expiration = $list['expiration'];
        $tokenPart = $list['token_participation'];
        $backURL = $app->urlFor('liste', ['token' => $tokenPart]);
        $itemArray = $item->toArray();
        $img = $itemArray['img'];
        $nom = $itemArray['nom'];
        $tarif = $itemArray['tarif'];
        $itemDesc = $itemArray['descr'];
        $itemURL = $itemArray['url'];
        $itemID = $itemArray['id'];
        $stringRes = <<< EOF
<a class="backButton" href=$backURL>Retour à la liste</a>
            <div class="itemDetail">

EOF;

        if (filter_var($img, FILTER_VALIDATE_URL)) {
            $stringRes .= <<< EOF
                <section class="itemImg"><img src="$img" alt="$nom"></section>

EOF;
        } else {
            $stringRes .= <<< EOF
                <section class="itemImg"><img src="$imgUri/$img" alt="$nom"></section>
            
EOF;
        }

        $stringRes .= <<< EOF
    <section class="nom"><strong>$nom</strong></section>
                <section class="itemDesc">$itemDesc</section>
                <section class="tarif">$tarif €</section>

EOF;

        if (filter_var($itemURL, FILTER_VALIDATE_URL)) {
            $host = parse_url($itemURL, PHP_URL_HOST);
            $stringRes .= <<< EOF
                <div class="itemURLDiv">
                    <p class="urlHeader">Lien vers le site de l'item : </p>
                    <a class="itemURL" href="$itemURL">$host</a>
                </div>

EOF;
        }

        if (is_null($item['reservation_id'])) {
            if ($item['cagnotte_active']) {
                $restant = $item['tarif'] - $item['cagnotte_montant'];
                if ($restant > 0) {
                    $partURL = $app->urlFor('participer-item', ['token' => $tokenPart, 'id' => $itemID]);
                    $stringRes .= <<< EOF
                <section class="reservation">Plus que $restant € pour remplir la cagnotte !</section>
                <a id="reserveLink" href="$partURL">Participez !</a>
            </div>

EOF;
                } else {
                    $stringRes .= <<< EOF
                    <section class="reservation">La cagnotte sur cet item a été remplie !</section>
                </div>
    
EOF;
                }
            } else {
                $reserveItem = $app->urlFor('reserver-item', ['token' => $tokenPart, 'id' => $itemID]);
                $stringRes .= <<< EOF
                <section class="reservation">Cet item n'a pas encore été reservé.</section>
                <a id="reserveLink" href="$reserveItem">Réservez-le !</a>
            </div>

EOF;
            }
        } else {

            $listCreator = false;

            if (isset($_SESSION['profile'])) {
                if ($list['user_id'] == $_SESSION['profile']['user_id'])
                    $listCreator = true;
            } else {
                if (isset($_COOKIE['listCreator'])) {
                    $cookieData = json_decode($_COOKIE['listCreator']);
                    foreach ($cookieData as $listID) {
                        if ($listID == $list['no']) {
                            $listCreator = true;
                            break;
                        }
                    }
                }
            }

            if ($listCreator && $today < $expiration) {
                $stringRes .= <<< EOF
                    <section class ="reservation">Cet item a été reservé. Revenez après la date d'expiration pour savoir par qui !</section>
                </div>

EOF;
            } else {
                $partName = $item->reservation()->first()->nom_participant;
                $message = $item->reservation()->first()->message;
                $stringRes .= <<< EOF
                <section class ="reservation">Cet item a été reservé par <strong>$partName</strong>.</section>
            </div>

EOF;
            }

            if ($listCreator && $today >= $expiration)
                $stringRes .= <<< EOF
            <div class="reservationMessage">
                <section class ="messageHeader"><strong>$partName</strong> vous a laissé un message :</section>
                <section class ="itemMessage">$message</section>
            </div>
EOF;
        }


        return $stringRes;
    }

    public function render()
    {
        switch ($this->selecteur) {
            case USER_LISTS_VIEW:
                $content = $this->userListsHTML($this->item);
                break;
            case LIST_VIEW:
                $content = $this->listHTML($this->item);
                break;
            case ITEM_VIEW:
                $content = $this->itemHTML($this->item);
                break;
            case PUBLIC_LISTS_VIEW:
                $content = $this->publicListsHTML($this->item);
                break;
            case LIST_MOD_VIEW:
                $content = $this->listModHTML($this->item);
                break;
            default:
                break;
        }
        if ($this->selecteur == LIST_MOD_VIEW) {
            print <<< EOF

        <div id="modListContent">
            $content
        </div>

EOF;
        } else {
            print <<< EOF

        <div id="content">
            $content
        </div>

EOF;
        }
    }
}
