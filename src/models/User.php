<?php

namespace mywishlist\models;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function role()
    {
        return $this->belongsTo('\mywishlist\models\Role', 'role_id');
    }

    public function lists()
    {
        return $this->hasMany('\mywishlist\models\Liste', 'user_id');
    }
}
