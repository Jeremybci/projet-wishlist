<?php
session_start();

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use mywishlist\controllers\UserController;
use mywishlist\controllers\LoginController;
use mywishlist\controllers\RegistrationController;
use mywishlist\controllers\ListController;
use mywishlist\controllers\ItemController;

$db = new DB();
$db->addConnection(parse_ini_file("src/config/config.ini"));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();

$uc = new UserController();
$lc = new LoginController();
$rc = new RegistrationController();
$lic = new ListController();
$ic = new ItemController();

$app->get('/', function () use ($uc) {
    $uc->redirectHome();
});

$app->get('/accueil', function () use ($uc) {
    $uc->loadHome();
})->name('accueil');

$app->get('/login', function () use ($lc) {
    $lc->loadLogin();
})->name('login');

$app->post('/login', function () use ($lc) {
    $lc->checkLogin();
});

$app->get('/logout', function () use ($lc) {
    $lc->logout();
});

$app->get('/inscription/:no', function ($no) use ($rc) {
    $rc->registrationLinkList($no);
})->name('inscription-lien');

$app->get('/inscription', function () use ($rc) {
    $rc->loadRegistration();
})->name('inscription');

$app->post('/inscription', function () use ($rc) {
    $rc->checkRegistration();
});

$app->get('/lier-liste/:token', function ($token) use ($uc) {
    $uc->linkList($token);
})->name('lier-liste');

$app->get('/profil/:id', function ($id) use ($uc) {
    $uc->loadProfile($id);
})->name('profil');

$app->get('/profil/:id/changer-mdp', function ($id) use ($uc) {
    $uc->changePasswordForm($id);
})->name('changer-mdp');

$app->post('/profil/:id/changer-mdp', function ($id) use ($uc) {
    $uc->changePassword($id);
});

$app->get('/vos-listes', function () use ($uc) {
    $uc->loadUserLists();
});

$app->get('/nouvelle-liste', function () use ($lic) {
    $lic->newListForm();
});

$app->post('/nouvelle-liste', function () use ($lic) {
    $lic->createList();
});

$app->get('/liste/:token', function ($token) use ($lic) {
    $lic->loadListPage($token);
})->name('liste');

$app->get('/liste/:token/item/:id', function ($token, $id) use ($ic) {
    $ic->loadItemPage($id);
})->name('item');

$app->post('/liste/:token', function ($token) use ($lic) {
    $lic->addMessage($token);
});

$app->get('/liste/:token/reserver-item/:id', function ($token, $id) use ($ic) {
    $ic->loadReservationForm($id, $token);
})->name('reserver-item');

$app->post('/liste/:token/reserver-item/:id', function ($token, $id) use ($ic) {
    $ic->reserveItem($id, $token);
});

$app->get('/liste/:token/participer-item/:id', function ($token, $id) use ($ic) {
    $ic->loadPoolForm($id, $token);
})->name('participer-item');

$app->post('/liste/:token/participer-item/:id', function ($token, $id) use ($ic) {
    $ic->poolContribution($id, $token);
});

$app->get('/liste-modif/:token', function ($token) use ($lic) {
    $lic->loadListModPage($token);
})->name('liste-modif');

$app->get('/liste-modif/:token/editer-liste', function ($token) use ($lic) {
    $lic->loadListEditForm($token);
})->name('editer-liste');

$app->post('/liste-modif/:token/editer-liste', function ($token) use ($lic) {
    $lic->editList($token);
});

$app->get('/liste-modif/:token/ajouter-item', function ($token) use ($ic) {
    $ic->addItemForm();
})->name('ajout-item');

$app->post('/liste-modif/:token/ajouter-item', function ($token) use ($ic) {
    $ic->addItem($token);
});

$app->get('/liste-modif/:token/toggle-cagnotte/:id', function ($token, $id) use ($ic) {
    $ic->togglePool($id, $token);
})->name('toggle-cagnotte');

$app->get('/liste-modif/:token/toggle-public', function ($token) use ($lic) {
    $lic->togglePublic($token);
})->name('toggle-public');

$app->get('/liste-modif/:token/editer-item/:id', function ($token, $id) use ($ic) {
    $ic->loadEditItemForm($id, $token);
})->name('edition-item');

$app->post('/liste-modif/:token/editer-item/:id', function ($token, $id) use ($ic) {
    $ic->editItem($id, $token);
});

$app->post('/liste-modif/:token/supprimer-item/:id', function ($token, $id) use ($ic) {
    $ic->deleteItem($id, $token);
})->name('suppression-item');

$app->post('/liste-modif/:token/supprimer-liste/', function ($token) use ($lic) {
    $lic->deleteList($token);
})->name('suppression-liste');

$app->run();
