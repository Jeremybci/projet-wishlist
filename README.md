
# Projet Wishlist Jérémy Bonci et Lucas Vogel

## Données pour démonstration

### Utilisateurs déjà crées

*  Nom d'utilisateur : gooduser | Mot de passe : Goodpassword1

* Nom d'utilisateur : lucasvgl | Mot de passe : Motdepasse1

### Liste non liée à un compte

* Lien de participation : http://localhost/php/projet-wishlist/liste/81c6c09940d84f952806
* Lien de modification : http://localhost/php/projet-wishlist/liste-modif/9249bd012b0a679270f9

## Fonctionnalités du projet 

### Participant
- [x] 1. Afficher une liste de souhaits
- [x] 2. Afficher un item d'une liste
- [x] 3. Réserver un item
- [x] 4. Ajouter un message avec sa réservation
	* Seul le créateur de la liste peut voir le message, et seulement après la date d'expiration
- [x] 5. Ajouter un message sur une liste

### Créateur
- [x] 6. Créer une liste
- [x] 7. Modifier les informations générales d'une de ses listes
- [x] 8. Ajouter des items
- [x] 9. Modifier un item
- [x] 10. Supprimer un item
- [x] 11. Rajouter une image à un item
	* Il aurait fallu ajouter une sélection possible des images déjà sur le serveur
- [x] 12. Modifier une image d'un item
- [x] 13. Supprimer une image d'un item
	* Pas de bouton explicite, il faut effacer le champ dans la page d'edition de l'item
- [x] 14. Partager une liste
	*	modal avec copie du lien sur un bouton (JS)
- [x] 15. Consulter les réservations d'une de ses listes avant échéance
- [x] 16. Consulter les réservations et messages d'une de ses listes après échéance

### Extensions
- [x] 17. Créer un compte
- [x] 18. S'authentifier
- [x] 19. Modifier son compte
	* Une gestion d’avatars avait été prévue mais nous n’avons pas eu le temps de l’implémenter
- [x] 20. Rendre une liste publique
	*	possibilité de toggle sur la page de modification d’une liste
- [x] 21. Afficher les listes de souhaits publiques
	*	elles sont disponibles sur la page d’accueil
- [x] 22. Créer une cagnotte sur un item
- [x] 23 Participer à une cagnotte
	*	mais pas de nom de participant ou de message
- [ ] Uploader une image
- [ ] 25 Créer un compte participant
	*	Je ne comprends pas le but de cet extension, un créateur peut être participant et vice-versa
- [ ] 26 Afficher la liste des créateurs
- [ ] 27 Supprimer son compte
- [x] 28 Joindre des listes à son compte
	*	message et lien sur la page de modification + lien d’inscription sur la page de modification d’une liste (si non connecté) qui lie automatiquement la liste au compte créé

## Tableau de bord
Fonctionnalités | Contributeur
---------------|--------------
11, 12, 13, 17, 18, 19, 21, 28 |Lucas
1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 15, 16, 20, 22, 23, 24| Jérémy

## URL Webetu

[Lien vers le projet sur Webetu](https://webetu.iutnc.univ-lorraine.fr/www/bonci1u/projet-wishlist)

## Repository

[Lien vers le repositery Bitbucket](https://bitbucket.org/Jeremybci/projet-wishlist/src/master/)