'use strict'

function displayModal() {
    var modal = $('#URLModal');
    modal.fadeToggle(50);
}

$(document).ready(function () {
    var button = $('#modalButton');
    var close = $('#closeModal');
    var copyButton = $('#copyButton');
    var copyText = $('#copyText');
    button.click(function () {
        displayModal();
        $("body").addClass("modalIsOpen");
    });
    close.click(function () {
        displayModal();
        copyButton.find('i').removeClass("fas fa-clipboard-check").addClass("fas fa-copy");
        $("body").removeClass("modalIsOpen");
    });
    copyButton.click(function () {
        copyText.select();
        document.execCommand("copy");
        copyButton.find('i').removeClass("fas fa-copy").addClass("fas fa-clipboard-check");
    })
});
